package king.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import king.game.states.GameStateManager;
import king.game.states.MenuState;

public class MyGame extends ApplicationAdapter {
	public static final int WIDTH = 1200;
	public static final int HEIGHT = 640;

	public static final String TITLE = "King Of Fighters";

	private GameStateManager gms;
	private SpriteBatch batch;

	private Music music;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		gms = new GameStateManager();
		music = Gdx.audio.newMusic(Gdx.files.internal("BGM2.mp3"));
		music.setLooping(true);
		music.setVolume(0.3f);
		music.play();
		Gdx.gl.glClearColor(1, 0, 0, 1);
		gms.push(new MenuState(gms));
	}

	@Override
	public void render () {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gms.update(Gdx.graphics.getDeltaTime());
		gms.render(batch);
	}

	@Override
	public void dispose() {
		super.dispose();
		music.dispose();
	}
}
