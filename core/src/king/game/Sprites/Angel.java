package king.game.Sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

import java.awt.TextArea;

public class Angel {
    public static final int GRAVITY = -15;
    private Vector3 position;
    private Vector3 velosity;

    private static Texture angel;

    public Angel (int x, int y){
        position = new Vector3(x,y,0);
        velosity = new Vector3(0,0,0);
        angel = new Texture("Angel4.png");
    }

    public Vector3 getPosition() {
        return position;
    }

    public static Texture getAngel() {
        return angel;
    }

    public void update (float dt){
        if (position.y > 40)
        velosity.add(0,GRAVITY, 0);
        velosity.scl(dt);
        position.add(0,velosity.y,0);
        if (position.y < 40)
            position.y = 40;


        velosity.scl(1/dt);
    }
}
