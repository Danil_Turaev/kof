package king.game.states;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import king.game.MyGame;
import king.game.Sprites.Angel;

public class PlayState extends State{

    private Angel angel;
    private Texture bg;

    public PlayState(GameStateManager gsm) {
        super(gsm);
        angel = new Angel(100,300);
        camera.setToOrtho(false, MyGame.WIDTH , MyGame.HEIGHT );
        bg = new Texture("К1.png");

    }

    @Override
    protected void handleInput() {

    }

    @Override
    public void update(float dt) {
        angel.update(dt);
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(camera.combined);
        sb.begin();
        sb.draw(bg,camera.position.x - (camera.viewportWidth / 2), 0);
        sb.draw(Angel.getAngel(), angel.getPosition().x,angel.getPosition().y);
        sb.end();
    }

    @Override
    public void dispose() {

    }
}
